export class User {
  id?: number;
  createdOn?: Date;
  updatedOn?: Date;
  deletedAt?: Date;
  name: string;
  email: string;
  hashedPassword: string;
}
