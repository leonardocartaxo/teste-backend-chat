import { ForbiddenException, Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as crypto from 'crypto';
import { UserCreateDto, UserDto } from './dtos/userDto';
import { AuthenticationResponse, IJwtPayload, Login } from './dtos/authDtos';
import { ClientProxy } from '@nestjs/microservices';
import { User } from './entities/user.entity';

const SALT = '1d8b84e4a115eea3f32ea772070238ab832bcd72b2fb59566c6e13e21c5d99db';

@Injectable()
export class AuthService {
  constructor(
    @Inject('USER_SERVICE') private readonly userServiceClient: ClientProxy,
    private readonly jwtService: JwtService,
  ) {}

  async signup(userCreateDto: UserCreateDto): Promise<AuthenticationResponse> {
    const user: User = {
      hashedPassword: AuthService.encryptPassword(userCreateDto.password),
      email: userCreateDto.email,
      name: userCreateDto.name,
    };
    console.log('user: ' + user);
    const userDto = await this.userServiceClient
      .send('user_create', user)
      .toPromise();

    const cleanedUserDto = new UserDto(userDto as User);

    return this.generateAuthenticationResponse(cleanedUserDto);
  }
  async login(userLogin: Login): Promise<AuthenticationResponse> {
    const user = await this.userServiceClient
      .send('user_find_one_by_email', userLogin.email)
      .toPromise();

    if (!user) throw new ForbiddenException();

    const isValidPassword = AuthService.verifyPassword(
      userLogin.password,
      user.hashedPassword,
    );

    if (!isValidPassword) throw new ForbiddenException();

    const userDto = new UserDto(user);

    return this.generateAuthenticationResponse(userDto);
  }

  private static encryptPassword(password: string): string {
    return crypto.createHmac('sha1', SALT).update(password).digest('hex');
  }

  private static verifyPassword(
    password: string,
    hashedPassword: string,
  ): boolean {
    return hashedPassword == AuthService.encryptPassword(password);
  }

  private generateAuthenticationResponse(
    userDto: UserDto,
  ): AuthenticationResponse {
    const jwtPayload: IJwtPayload = {
      id: userDto.id,
      email: userDto.email,
      name: userDto.name,
    };

    return {
      accessToken: this.jwtService.sign(jwtPayload),
      user: userDto as UserDto,
    };
  }
}
