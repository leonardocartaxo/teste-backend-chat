import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Request,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { MessagePattern } from '@nestjs/microservices';
import { UserCreateDto } from './dtos/userDto';
import { AuthenticationResponse, Login } from './dtos/authDtos';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @MessagePattern('auth_signup')
  async create(
    @Body() createUserDto: UserCreateDto,
  ): Promise<AuthenticationResponse> {
    try {
      return await this.authService.signup(createUserDto);
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  @MessagePattern('auth_login')
  async login(@Body() userLogin: Login): Promise<AuthenticationResponse> {
    return await this.authService.login(userLogin);
  }

  @MessagePattern('auth_profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
