import { UserDto } from './userDto';

export class Login {
  email: string;
  password: string;
}

export class AuthenticationResponse {
  accessToken: string;
  user: UserDto;
}

export interface IJwtPayload {
  id: number;
  email: string;
  name: string;
  iat?: number;
  exp?: number;
}

export interface ICurrentUser {
  id: number;
  email: string;
  name: string;
}
