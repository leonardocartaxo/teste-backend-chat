import { User } from '../entities/user.entity';

export class UserDto {
  readonly id?: number;
  name: string;
  email: string;

  constructor(user: User) {
    this.id = user.id;
    this.name = user.name;
    this.email = user.email;
  }
}

// export type UserDto = Omit<User, "hashedPassword">;

export class UserCreateDto {
  name: string;
  email: string;
  password: string;
}

export class UserUpdateDto {
  name: string;
}

export class UserUpdateEmail {
  email: string;
}

export class UserUpdatePassword {
  password: string;
}
