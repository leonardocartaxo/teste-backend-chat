import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { UsersController } from './users/users.controller';
import DbConfig from '../ormconfig.js';

const dbConfig = DbConfig as any;
dbConfig.entities = [User];
dbConfig.migrations = null;
dbConfig.host = process.env.BTC_USERS_DB_HOST;

@Module({
  imports: [TypeOrmModule.forRoot(), UsersModule],
  controllers: [UsersController],
})
export class AppModule {}
