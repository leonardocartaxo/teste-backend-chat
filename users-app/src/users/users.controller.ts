import {
  ClassSerializerInterceptor,
  Controller,
  UseInterceptors,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDto } from './userDto';
import { MessagePattern } from '@nestjs/microservices';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern('user_find_all')
  async findAll(): Promise<UserDto[]> {
    return await this.usersService.findAll();
  }

  @MessagePattern('user_find_one')
  async findOne(id: number): Promise<UserDto> {
    return await this.usersService.findById(id);
  }

  @MessagePattern('user_find_one_by_email')
  async findOneByEmail(email: string): Promise<UserDto> {
    return await this.usersService.findOneByEmail(email);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern('user_create')
  async create(data): Promise<UserDto> {
    return await this.usersService.create(data);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern('user_update')
  async update(data): Promise<UserDto> {
    return await this.usersService.update(data.id, data.userDto);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @MessagePattern('user_delete')
  async delete(id: string) {
    return this.usersService.delete(id);
  }
}
