import { Controller } from '@nestjs/common';
import { ChatRoomMessage } from './entities/chat-room-message.entity';
import { ChatRoomMessageService } from './chat-room-message.service';
import { MessagePattern } from '@nestjs/microservices';
import {
  CreateChatRoomMessageDto,
  FilterChatRoomMessageDto,
} from './dtos/chat-room-message.dto';

@Controller('chat-room-messages')
export class ChatRoomMessagesController {
  constructor(
    private readonly chatRoomMessageService: ChatRoomMessageService,
  ) {}

  @MessagePattern('chat-room-messages_create')
  async create(
    createChatRoomMessageDto: CreateChatRoomMessageDto,
  ): Promise<ChatRoomMessage> {
    return await this.chatRoomMessageService.create(
      createChatRoomMessageDto.userId,
      createChatRoomMessageDto.chatRoomId,
      createChatRoomMessageDto.text,
    );
  }

  @MessagePattern('chat-room-messages_filter')
  async get(
    filterChatRoomMessageDto: FilterChatRoomMessageDto,
  ): Promise<ChatRoomMessage[]> {
    return await this.chatRoomMessageService.filter(
      filterChatRoomMessageDto.chatRoomId,
      filterChatRoomMessageDto.startDate,
      filterChatRoomMessageDto.endDate,
    );
  }
}
