export class CreateChatRoomMessageDto {
  userId: number;
  chatRoomId: number;
  text: string;
}

export class FilterChatRoomMessageDto {
  chatRoomId: number;
  startDate: Date;
  endDate: Date;
}
