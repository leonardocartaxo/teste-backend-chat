import { Test, TestingModule } from '@nestjs/testing';
import { ChatRoomMessagesController } from './chat-room-messages.controller';

describe('ChatRoomMessagesController', () => {
  let controller: ChatRoomMessagesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChatRoomMessagesController],
    }).compile();

    controller = module.get<ChatRoomMessagesController>(
      ChatRoomMessagesController,
    );
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
