import { Injectable } from '@nestjs/common';
import { ChatRoomMessage } from './entities/chat-room-message.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Between, Repository } from 'typeorm';

@Injectable()
export class ChatRoomMessageService {
  constructor(
    @InjectRepository(ChatRoomMessage)
    private chatRoomMessageRepository: Repository<ChatRoomMessage>,
  ) {}

  async create(
    userId: number,
    chatRoomId: number,
    text: string,
  ): Promise<ChatRoomMessage> {
    return await this.chatRoomMessageRepository.save({
      chatRoomId: chatRoomId,
      userId: userId,
      text: text,
    });
  }

  async filter(
    chatRoomId: number,
    startDate: Date,
    endDate: Date,
  ): Promise<ChatRoomMessage[]> {
    return await this.chatRoomMessageRepository.find({
      where: {
        chatRoomId: chatRoomId,
        createdOn: Between(startDate, endDate),
      },
    });
  }
}
