import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('chat-room-messages')
export class ChatRoomMessage {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @Column()
  chatRoomId: number;

  @Column()
  userId: number;

  @Column({ type: 'text' })
  text: string;
}
