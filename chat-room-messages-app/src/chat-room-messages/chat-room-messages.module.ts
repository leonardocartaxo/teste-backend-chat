import { Module } from '@nestjs/common';
import { ChatRoomMessageService } from './chat-room-message.service';
import { ChatRoomMessagesController } from './chat-room-messages.controller';
import { ChatRoomMessage } from './entities/chat-room-message.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ChatRoomMessage])],
  providers: [ChatRoomMessageService],
  controllers: [ChatRoomMessagesController],
})
export class ChatRoomMessagesModule {}
