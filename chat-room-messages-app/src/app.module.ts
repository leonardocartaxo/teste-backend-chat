import { Module } from '@nestjs/common';
import { ChatRoomMessagesModule } from './chat-room-messages/chat-room-messages.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatRoomMessage } from './chat-room-messages/entities/chat-room-message.entity';
import DbConfig from '../ormconfig.json';

const dbConfig = DbConfig as any;
dbConfig.entities = [ChatRoomMessage];
dbConfig.migrations = null;
dbConfig.synchronize = true;

@Module({
  imports: [TypeOrmModule.forRoot(dbConfig), ChatRoomMessagesModule],
})
export class AppModule {}
