import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Inject,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { ClientProxy } from '@nestjs/microservices';
import {
  ChatRoomMessageDto,
  CreateChatRoomMessageDto,
  FilterChatRoomMessageDto,
} from '../dtos/chat-room-message.dto';

@ApiTags('chat-room-messages')
@Controller('chat-room-messages')
export class ChatRoomMessagesController {
  constructor(
    @Inject('CHAT-ROOM-MESSAGE_SERVICE')
    private readonly chatRoomMessageServiceClient: ClientProxy,
  ) {}

  @Post()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Send a message' })
  @ApiResponse({ status: 200, type: ChatRoomMessageDto })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Body() createChatRoomMessageDto: CreateChatRoomMessageDto,
  ): Promise<ChatRoomMessageDto> {
    try {
      return await this.chatRoomMessageServiceClient
        .send('chat-room-messages_create', createChatRoomMessageDto)
        .toPromise();
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  @Post('filter')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  async login(
    @Body() filterChatRoomMessageDto: FilterChatRoomMessageDto,
  ): Promise<ChatRoomMessageDto[]> {
    return await this.chatRoomMessageServiceClient
      .send('chat-room-messages_filter', filterChatRoomMessageDto)
      .toPromise();
  }
}
