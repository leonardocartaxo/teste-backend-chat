import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Put,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ClientProxy } from '@nestjs/microservices';
import { UserDto, UserUpdateDto } from '../dtos/userDto';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(
    @Inject('USER_SERVICE') private readonly userServiceClient: ClientProxy,
  ) {}

  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get all users' })
  @ApiResponse({
    status: 200,
    type: [UserDto],
  })
  async findAll(): Promise<any[]> {
    return await this.userServiceClient.send('user_find_all', {}).toPromise();
  }

  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Find one user by id' })
  @ApiResponse({
    status: 200,
    type: UserDto,
  })
  async findOne(@Param('id') id: number): Promise<UserDto> {
    return await this.userServiceClient.send('user_find_one', id).toPromise();
  }

  @Put(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Update one user by id' })
  @ApiResponse({
    status: 200,
    type: UserUpdateDto,
  })
  async update(
    @Param('id') id: string,
    @Body() userDto: UserDto,
  ): Promise<UserDto> {
    return await this.userServiceClient
      .send<UserDto>('user_update', { id, userDto })
      .toPromise();
  }

  @Delete(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Delete one user by id' })
  @ApiResponse({
    status: 200,
    description: 'delete user by Id',
  })
  async delete(@Param('id') id: string) {
    return await this.userServiceClient.send('user_delete', id).toPromise();
  }
}
