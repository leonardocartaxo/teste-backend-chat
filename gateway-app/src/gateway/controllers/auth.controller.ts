import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Inject,
  Post,
  Request,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import { AuthenticationResponse, Login } from '../dtos/authDtos';
import { UserCreateDto, UserDto } from '../dtos/userDto';
import { ClientProxy } from '@nestjs/microservices';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(
    @Inject('AUTH_SERVICE') private readonly authServiceClient: ClientProxy,
  ) {}

  @Post('signup')
  @ApiOperation({ summary: 'Create account' })
  @ApiResponse({ status: 200, type: UserDto })
  @ApiResponse({ status: 403, description: 'Forbidden.' })
  async create(
    @Body() createUserDto: UserCreateDto,
  ): Promise<AuthenticationResponse> {
    try {
      return await this.authServiceClient
        .send('auth_signup', createUserDto)
        .toPromise();
    } catch (error) {
      throw new HttpException(error, HttpStatus.BAD_REQUEST);
    }
  }

  @Post('login')
  @UseInterceptors(ClassSerializerInterceptor)
  async login(@Body() userLogin: Login): Promise<AuthenticationResponse> {
    return await this.authServiceClient
      .send('auth_login', userLogin)
      .toPromise();
  }

  @Get('profile')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth()
  getProfile(@Request() req) {
    return req.user;
  }
}
