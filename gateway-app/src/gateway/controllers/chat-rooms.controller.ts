import {
  Body,
  Controller,
  Get,
  Inject,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { ClientProxy } from '@nestjs/microservices';
import { JwtAuthGuard } from '../guards/jwt-auth.guard';
import {
  ChatRoomDto,
  CreateChatRoomDto,
  JoinChatRoomDto,
} from '../dtos/ChatRoomDto';

@ApiTags('chat-rooms')
@Controller('chat-rooms')
export class ChatRoomsController {
  constructor(
    @Inject('CHAT-ROOM_SERVICE')
    private readonly chatRoomServiceClient: ClientProxy,
  ) {}

  @Get()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get all chat rooms' })
  @ApiResponse({
    status: 200,
    type: [ChatRoomDto],
  })
  async findAll(): Promise<ChatRoomDto[]> {
    return await this.chatRoomServiceClient
      .send('chat-rooms_find_all', {})
      .toPromise();
  }

  @Get(':id')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Find one chat room by id' })
  @ApiResponse({
    status: 200,
    type: ChatRoomDto,
  })
  async findOne(@Param('id') id: number): Promise<ChatRoomDto> {
    return await this.chatRoomServiceClient
      .send('chat-rooms_find_by_id', id)
      .toPromise();
  }

  @Post()
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Create chat room' })
  @ApiResponse({
    status: 200,
    type: ChatRoomDto,
  })
  async create(
    @Body() createChatRoomDto: CreateChatRoomDto,
  ): Promise<ChatRoomDto> {
    return await this.chatRoomServiceClient
      .send('chat-rooms_create', createChatRoomDto.userId)
      .toPromise();
  }

  @Post('join')
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Create chat room' })
  @ApiResponse({
    status: 200,
    type: ChatRoomDto,
  })
  async join(@Body() joinChatRoomDto: JoinChatRoomDto): Promise<ChatRoomDto> {
    return await this.chatRoomServiceClient
      .send('chat-rooms_join', joinChatRoomDto)
      .toPromise();
  }
}
