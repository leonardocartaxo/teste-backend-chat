import { Transport } from '@nestjs/microservices';

process.env.API_GATEWAY_PORT = process.env.API_GATEWAY_PORT || `3000`;
process.env.USER_SERVICE_PORT = process.env.USER_SERVICE_PORT || `3001`;
process.env.USER_SERVICE_HOST = process.env.USER_SERVICE_HOST || `user`;

export class ConfigService {
  private readonly envConfig: { [key: string]: any } = null;

  constructor() {
    this.envConfig = {};
    this.envConfig.port = process.env.API_GATEWAY_PORT;
    this.envConfig.userService = {
      options: {
        port: process.env.USER_SERVICE_PORT,
        host: process.env.USER_SERVICE_HOST,
      },
      transport: Transport.TCP,
    };
  }

  get(key: string): any {
    return this.envConfig[key];
  }
}
