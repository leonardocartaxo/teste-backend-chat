import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { UsersController } from './controllers/users.controller';
import { AuthController } from './controllers/auth.controller';
import { ChatRoomsController } from './controllers/chat-rooms.controller';
import { JwtStrategy } from './strategies/jwt.strategy';
import { ChatRoomMessagesController } from './controllers/chat-room-messages.controller';

process.env.BTC_USERS_HOST = process.env.BTC_USERS_HOST || 'localhost';
process.env.BTC_USERS_PORT = process.env.BTC_USERS_PORT || '3001';

process.env.BTC_AUTH_HOST = process.env.BTC_AUTH_HOST || 'localhost';
process.env.BTC_AUTH_PORT = process.env.BTC_AUTH_PORT || '3002';

process.env.BTC_CHAT_ROOM_HOST = process.env.BTC_CHAT_ROOM_HOST || 'localhost';
process.env.BTC_CHAT_ROOM_PORT = process.env.BTC_CHAT_ROOM_PORT || '3003';

process.env.BTC_CHAT_ROOM_MESSAGE_HOST =
  process.env.BTC_CHAT_ROOM_MESSAGE_HOST || 'localhost';
process.env.BTC_CHAT_ROOM_MESSAGE_PORT =
  process.env.BTC_CHAT_ROOM_MESSAGE_PORT || '3004';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'USER_SERVICE',
        transport: Transport.TCP,
        options: {
          host: process.env.BTC_USERS_HOST,
          port: Number(process.env.BTC_USERS_PORT),
        },
      },
      {
        name: 'AUTH_SERVICE',
        transport: Transport.TCP,
        options: {
          host: process.env.BTC_AUTH_HOST,
          port: Number(process.env.BTC_AUTH_PORT),
        },
      },
      {
        name: 'CHAT-ROOM_SERVICE',
        transport: Transport.TCP,
        options: {
          host: process.env.BTC_CHAT_ROOM_HOST,
          port: Number(process.env.BTC_CHAT_ROOM_PORT),
        },
      },
      {
        name: 'CHAT-ROOM-MESSAGE_SERVICE',
        transport: Transport.TCP,
        options: {
          host: process.env.BTC_CHAT_ROOM_MESSAGE_HOST,
          port: Number(process.env.BTC_CHAT_ROOM_MESSAGE_PORT),
        },
      },
    ]),
  ],
  providers: [JwtStrategy],
  controllers: [
    UsersController,
    AuthController,
    ChatRoomsController,
    ChatRoomMessagesController,
  ],
})
export class GatewayModule {}
