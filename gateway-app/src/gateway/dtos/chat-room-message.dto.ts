import { ApiProperty } from '@nestjs/swagger';

export class ChatRoomMessageDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  createdOn: Date;
  @ApiProperty()
  updatedOn: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  userId: number;
  @ApiProperty()
  chatRoomId: number;
  @ApiProperty()
  text: string;
}

export class CreateChatRoomMessageDto {
  @ApiProperty()
  userId: number;
  @ApiProperty()
  chatRoomId: number;
  @ApiProperty()
  text: string;
}

export class FilterChatRoomMessageDto {
  @ApiProperty()
  chatRoomId: number;
  @ApiProperty()
  startDate: Date;
  @ApiProperty()
  endDate: Date;
}
