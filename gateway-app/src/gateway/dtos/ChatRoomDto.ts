import { ApiProperty } from '@nestjs/swagger';

export class ChatRoomDto {
  @ApiProperty()
  id: number;
  @ApiProperty()
  createdOn: Date;
  @ApiProperty()
  updatedOn: Date;
  @ApiProperty()
  deletedAt?: Date;
  ChatRoomTeamMembers: ChatRoomTeamMemberDto[];
}

export class ChatRoomTeamMemberDto {
  @ApiProperty()
  id?: number;
  @ApiProperty()
  createdOn: Date;
  @ApiProperty()
  updatedOn: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  chatRoom: ChatRoomDto;
  @ApiProperty()
  useId: number;
  @ApiProperty()
  type: ChatRoomTeamMemberTypeDto = ChatRoomTeamMemberTypeDto.guest;
}

enum ChatRoomTeamMemberTypeDto {
  owner = 'OWNER',
  guest = 'GUEST',
}

export class CreateChatRoomDto {
  @ApiProperty()
  userId: number;
}

export class JoinChatRoomDto {
  @ApiProperty()
  chatRoomId: number;
  @ApiProperty()
  userId: number;
}
