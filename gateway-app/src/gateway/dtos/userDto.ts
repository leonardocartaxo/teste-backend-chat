import { ApiProperty } from '@nestjs/swagger';

export class UserDto {
  @ApiProperty()
  readonly id?: number;
  @ApiProperty()
  createdOn: Date;
  @ApiProperty()
  updatedOn: Date;
  @ApiProperty()
  deletedAt?: Date;
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
}

export class UserCreateDto {
  @ApiProperty()
  name: string;
  @ApiProperty()
  email: string;
  @ApiProperty()
  password: string;
}

export class UserUpdateDto {
  @ApiProperty()
  name: string;
}

export class UserUpdateEmail {
  @ApiProperty()
  email: string;
}

export class UserUpdatePassword {
  @ApiProperty()
  password: string;
}
