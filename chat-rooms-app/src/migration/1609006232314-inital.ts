import { MigrationInterface, QueryRunner } from 'typeorm';

export class inital1609006232314 implements MigrationInterface {
  name = 'inital1609006232314';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`CREATE TABLE "chat-rooms"
                                 (
                                     "id"        SERIAL    NOT NULL,
                                     "createdOn" TIMESTAMP NOT NULL DEFAULT now(),
                                     "updatedOn" TIMESTAMP NOT NULL DEFAULT now(),
                                     "deletedAt" TIMESTAMP,
                                     CONSTRAINT "PK_e4005b2d7c7fcdb9560f72979a3" PRIMARY KEY ("id")
                                 )`);
    await queryRunner.query(`CREATE TABLE "chat-room-team-members"
                                 (
                                     "id"         SERIAL            NOT NULL,
                                     "createdOn"  TIMESTAMP         NOT NULL DEFAULT now(),
                                     "updatedOn"  TIMESTAMP         NOT NULL DEFAULT now(),
                                     "deletedAt"  TIMESTAMP,
                                     "user"       integer           NOT NULL,
                                     "type"       character varying NOT NULL,
                                     "chatRoomId" integer,
                                     CONSTRAINT "PK_06d88159a61c5037cee27ec0e44" PRIMARY KEY ("id")
                                 )`);
    await queryRunner.query(`ALTER TABLE "chat-room-team-members"
            ADD CONSTRAINT "FK_11f6d95d4c91a2d8550293da376" FOREIGN KEY ("chatRoomId") REFERENCES "chat-rooms" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "chat-room-team-members" DROP CONSTRAINT "FK_11f6d95d4c91a2d8550293da376"`,
    );
    await queryRunner.query(`DROP TABLE "chat-room-team-members"`);
    await queryRunner.query(`DROP TABLE "chat-rooms"`);
  }
}
