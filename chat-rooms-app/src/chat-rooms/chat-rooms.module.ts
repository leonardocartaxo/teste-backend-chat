import { Module } from '@nestjs/common';
import { ChatRoomsService } from './chat-rooms.service';
import { ChatRoomsController } from './chat-rooms.controller';
import { ChatRoom } from './entities/chat-room.entity';
import { ChatRoomTeamMember } from './entities/chat-room-team-members.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([ChatRoom, ChatRoomTeamMember])],
  providers: [ChatRoomsService],
  controllers: [ChatRoomsController],
})
export class ChatRoomsModule {}
