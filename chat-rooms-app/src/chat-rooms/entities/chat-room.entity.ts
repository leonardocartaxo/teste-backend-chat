import {
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChatRoomTeamMember } from './chat-room-team-members.entity';

@Entity('chat-rooms')
export class ChatRoom {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @OneToMany(
    () => ChatRoomTeamMember,
    (ChatRoomTeamMember) => ChatRoomTeamMember.chatRoom,
  )
  ChatRoomTeamMembers: ChatRoomTeamMember[];
}
