import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ChatRoom } from './chat-room.entity';

@Entity('chat-room-team-members')
export class ChatRoomTeamMember {
  @PrimaryGeneratedColumn()
  id?: number;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @DeleteDateColumn()
  deletedAt?: Date;

  @ManyToOne(() => ChatRoom)
  chatRoom: ChatRoom;

  @Column({ nullable: false })
  userId: number;

  @Column({ nullable: false })
  type: ChatRoomTeamMemberType = ChatRoomTeamMemberType.guest;
}

export enum ChatRoomTeamMemberType {
  owner = 'OWNER',
  guest = 'GUEST',
}
