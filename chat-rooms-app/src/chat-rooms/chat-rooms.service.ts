import { Injectable } from '@nestjs/common';
import { ChatRoom } from './entities/chat-room.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import {
  ChatRoomTeamMember,
  ChatRoomTeamMemberType,
} from './entities/chat-room-team-members.entity';

@Injectable()
export class ChatRoomsService {
  constructor(
    @InjectRepository(ChatRoom)
    private chatRoomRepository: Repository<ChatRoom>,
    @InjectRepository(ChatRoomTeamMember)
    private chatRoomTeamMemberRepository: Repository<ChatRoomTeamMember>,
  ) {}
  async create(userId: number): Promise<ChatRoom> {
    const chatRoom = await this.chatRoomRepository.save({});
    await this.chatRoomTeamMemberRepository.save({
      chatRoom: chatRoom,
      userId: userId,
      type: ChatRoomTeamMemberType.owner,
    });

    return await this.findById(chatRoom.id);
  }

  async findById(id: number): Promise<ChatRoom> {
    return await this.chatRoomRepository.findOne(id, {
      relations: ['ChatRoomTeamMembers'],
    });
  }

  async join(chatRoomId: number, userId: number): Promise<ChatRoom> {
    const chatRoom = await this.chatRoomRepository.findOne(chatRoomId);
    await this.chatRoomTeamMemberRepository.save({
      chatRoom: chatRoom,
      userId: userId,
      type: ChatRoomTeamMemberType.guest,
    });

    return await this.findById(chatRoom.id);
  }

  async findAll(): Promise<ChatRoom[]> {
    return await this.chatRoomRepository.find({
      relations: ['ChatRoomTeamMembers'],
    });
  }
}
