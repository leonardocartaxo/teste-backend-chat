import { Controller } from '@nestjs/common';
import { ChatRoomsService } from './chat-rooms.service';
import { MessagePattern } from '@nestjs/microservices';
import { ChatRoom } from './entities/chat-room.entity';
import { JoinChatRoomDto } from './dtos/chat-room.dto';

@Controller('chat-rooms')
export class ChatRoomsController {
  constructor(private readonly chatRoomsService: ChatRoomsService) {}

  @MessagePattern('chat-rooms_find_all')
  async findAll(): Promise<ChatRoom[]> {
    return await this.chatRoomsService.findAll();
  }

  @MessagePattern('chat-rooms_create')
  async create(userId: number): Promise<ChatRoom> {
    return await this.chatRoomsService.create(userId);
  }

  @MessagePattern('chat-rooms_find_by_id')
  async findById(chatRoomId: number): Promise<ChatRoom> {
    return await this.chatRoomsService.findById(chatRoomId);
  }

  @MessagePattern('chat-rooms_join')
  async join(joinChatRoomDto: JoinChatRoomDto): Promise<ChatRoom> {
    return await this.chatRoomsService.join(
      joinChatRoomDto.chatRoomId,
      joinChatRoomDto.userId,
    );
  }
}
