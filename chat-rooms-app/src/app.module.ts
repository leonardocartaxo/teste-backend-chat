import { Module } from '@nestjs/common';
import { ChatRoomsModule } from './chat-rooms/chat-rooms.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ChatRoom } from './chat-rooms/entities/chat-room.entity';
import { ChatRoomTeamMember } from './chat-rooms/entities/chat-room-team-members.entity';
import DbConfig from '../ormconfig.json';

const dbConfig = DbConfig as any;
dbConfig.entities = [ChatRoom, ChatRoomTeamMember];
dbConfig.migrations = null;
dbConfig.synchronize = true;

@Module({
  imports: [TypeOrmModule.forRoot(dbConfig), ChatRoomsModule],
})
export class AppModule {}
